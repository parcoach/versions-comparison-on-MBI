     1	// DO NOT EDIT: this file was generated by /MBI/scripts/CollArgGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: Collective MPI_Exscan with an invalid datatype 
     7	    Collective MPI_Exscan with an invalid datatype 
     8	
     9		Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Yes
    16		COLL!nonblocking: Lacking
    17		COLL!persistent: Lacking
    18		COLL!tools: Lacking  
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: InvalidDatatype
    25	  | Invalid Datatype. MPI_Exscan at CollDataNull_Exscan_nok.c:59 has an invalid datatype.
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	#define buff_size 128
    34	
    35	int main(int argc, char **argv) {
    36	  int nprocs = -1;
    37	  int rank = -1;
    38		int root = 0;
    39		int size = 1, j=0, color=0;
    40	
    41	  MPI_Init(&argc, &argv);
    42	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    43	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    44	  printf("Hello from rank %d \n", rank);
    45	
    46	  if (nprocs < 2)
    47	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    48	
    49		MPI_Comm newcom = MPI_COMM_WORLD;
    50		MPI_Op op = MPI_SUM;
    51		MPI_Datatype type = MPI_INT;
    52	
    53		int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */  
    54	
    55	  int outbuf1[buff_size], inbuf1[buff_size];
    56	  
    57	
    58	  type=MPI_DATATYPE_NULL; /* MBIERROR1 */
    59	  MPI_Exscan(&outbuf1, inbuf1, buff_size, type, op, newcom); /* MBIERROR2 */  
    60		
    61		
    62	
    63	  MPI_Finalize();
    64	  printf("Rank %d finished normally\n", rank);
    65	  return 0;
    66	}
