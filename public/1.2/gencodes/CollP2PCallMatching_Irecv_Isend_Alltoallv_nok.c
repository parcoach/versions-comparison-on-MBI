// DO NOT EDIT: this file was generated by /MBI/scripts/CollP2PMatchingGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Point to point & collective mismatch
    Point to point MPI_Irecv is matched with MPI_Alltoallv which causes a deadlock.

	 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation

BEGIN_MPI_FEATURES
	P2P!basic: Lacking 
	P2P!nonblocking: Yes
	P2P!persistent: Lacking
	COLL!basic: Yes 
	COLL!nonblocking: Lacking
	COLL!persistent: Lacking
	COLL!tools: Lacking
	RMA: Lacking
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | ERROR: CallMatching
  | P2P & Collective mistmatch. MPI_Irecv at CollP2PCallMatching_Irecv_Isend_Alltoallv_nok.c:72 is matched with MPI_Alltoallv at CollP2PCallMatching_Irecv_Isend_Alltoallv_nok.c:66 wich causes a deadlock.
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
  int dest, src;
  int root = 0;
	int stag = 0, rtag = 0;
	int buff_size = 1;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");

  int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
	MPI_Comm newcom = MPI_COMM_WORLD;
	MPI_Datatype type = MPI_INT;
	MPI_Op op = MPI_SUM;  

  int buf1=rank; MPI_Request req1=MPI_REQUEST_NULL;
  int buf2=-1; MPI_Request req2=MPI_REQUEST_NULL;
  int *sbuf3=(int*)malloc(dbs*2), *rbuf3=(int*)malloc(dbs*2), *scounts3=(int*)malloc(dbs), *rcounts3=(int*)malloc(dbs), *sdispls3=(int*)malloc(dbs), *rdispls3=(int*)malloc(dbs);
  for (int i = 0; i < nprocs; i++) {
    scounts3[i] = 2;
    rcounts3[i] = 2;
    sdispls3[i] = (nprocs - (i + 1)) * 2;
    rdispls3[i] = i * 2;
  }
	if (rank == 0) {
		dest=1;src=1;
  	MPI_Alltoallv(sbuf3, scounts3, sdispls3, type, rbuf3, rcounts3, rdispls3, type, newcom); /* MBIERROR1 */
		
  	MPI_Isend(&buf1, buff_size, type, dest, stag, newcom, &req1); 
		MPI_Wait(&req1, MPI_STATUS_IGNORE);
	}else if (rank==1) {
		dest=0;src=0;
  	MPI_Irecv(&buf2, buff_size, type, src, rtag, newcom, &req2); /* MBIERROR2 */
		 MPI_Wait(&req2, MPI_STATUS_IGNORE);
  	MPI_Alltoallv(sbuf3, scounts3, sdispls3, type, rbuf3, rcounts3, rdispls3, type, newcom); 
		
	}

	if(req1 != MPI_REQUEST_NULL) MPI_Request_free(&req1);
	if(req2 != MPI_REQUEST_NULL) MPI_Request_free(&req2);
	free(sbuf3);free(rbuf3);free(scounts3);free(rcounts3);free(sdispls3);free(rdispls3);
  
	MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
