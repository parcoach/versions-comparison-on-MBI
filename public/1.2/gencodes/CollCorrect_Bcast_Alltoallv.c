// DO NOT EDIT: this file was generated by /MBI/scripts/CollMatchingGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Correct collective ordering
    All ranks call MPI_Bcast and then MPI_Alltoallv

	 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation

BEGIN_MPI_FEATURES
	P2P!basic: Lacking
	P2P!nonblocking: Lacking
	P2P!persistent: Lacking
	COLL!basic: Yes
	COLL!nonblocking: Lacking
	COLL!persistent: Lacking
	COLL!tools: Lacking
	RMA: Lacking
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | OK
  | 
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define buff_size 128

int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
	int root = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug.\n");

	MPI_Comm newcom = MPI_COMM_WORLD;
	MPI_Datatype type = MPI_INT;
  MPI_Op op = MPI_SUM;

  int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
  int buf1[buff_size];
  int *sbuf2=(int*)malloc(dbs*2), *rbuf2=(int*)malloc(dbs*2), *scounts2=(int*)malloc(dbs), *rcounts2=(int*)malloc(dbs), *sdispls2=(int*)malloc(dbs), *rdispls2=(int*)malloc(dbs);
  for (int i = 0; i < nprocs; i++) {
    scounts2[i] = 2;
    rcounts2[i] = 2;
    sdispls2[i] = (nprocs - (i + 1)) * 2;
    rdispls2[i] = i * 2;
  }

  if (rank % 2) {
    MPI_Bcast(buf1, buff_size, type, root, newcom); /* MBIERROR1 */
  	
    MPI_Alltoallv(sbuf2, scounts2, sdispls2, type, rbuf2, rcounts2, rdispls2, type, newcom);
  	
  } else {
    MPI_Bcast(buf1, buff_size, type, root, newcom); /* MBIERROR2 */
  	
    MPI_Alltoallv(sbuf2, scounts2, sdispls2, type, rbuf2, rcounts2, rdispls2, type, newcom);
  	
  }

  
  free(sbuf2);free(rbuf2);free(scounts2);free(rcounts2);free(sdispls2);free(rdispls2);
  
  MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
