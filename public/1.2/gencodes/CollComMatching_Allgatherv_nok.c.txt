     1	// DO NOT EDIT: this file was generated by /MBI/scripts/CollComGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: Collective MPI_Allgatherv with a communicator mismatch
     7	    Odd ranks call the collective on newcom while even ranks call the collective on MPI_COMM_WORLD
     8	
     9		 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Yes
    16		COLL!nonblocking: Lacking
    17		COLL!persistent: Lacking
    18		COLL!tools: Yes
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: CommunicatorMatching
    25	  | Communicator mistmatch in collectives. MPI_Allgatherv at CollComMatching_Allgatherv_nok.c:54 has newcom or MPI_COMM_WORLD as a communicator.
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	#define buff_size 128
    34	
    35	int main(int argc, char **argv) {
    36	  int nprocs = -1;
    37	  int rank = -1;
    38		int root = 0;
    39	
    40	  MPI_Init(&argc, &argv);
    41	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    42	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    43	  printf("Hello from rank %d \n", rank);
    44	
    45	  if (nprocs < 2)
    46	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    47	  
    48	  MPI_Op op = MPI_SUM;
    49	  MPI_Datatype type = MPI_INT;
    50		MPI_Comm newcom;
    51	  MPI_Comm_split(MPI_COMM_WORLD, 0, nprocs - rank, &newcom);
    52	
    53	  if (rank % 2)
    54	    newcom = MPI_COMM_WORLD; /* MBIERROR */
    55	
    56		int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
    57	  int *rbuf1 = (int*)malloc(dbs*2), *rcounts1=(int*)malloc(dbs),  *displs1=(int*)malloc(dbs);
    58	  for (int i = 0; i < nprocs; i++) {
    59	    rcounts1[i] = 1;
    60	    displs1[i] = 2 * (nprocs - (i + 1));
    61	  }
    62	  
    63	  MPI_Allgatherv(&rank, 1, type, rbuf1, rcounts1, displs1, type, newcom); /* MBIERROR */ 
    64		
    65		free(rbuf1);free(rcounts1);free(displs1);
    66	
    67		if(newcom != MPI_COMM_NULL && newcom != MPI_COMM_WORLD)
    68			MPI_Comm_free(&newcom);
    69	
    70	  MPI_Finalize();
    71	  printf("Rank %d finished normally\n", rank);
    72	  return 0;
    73	}
