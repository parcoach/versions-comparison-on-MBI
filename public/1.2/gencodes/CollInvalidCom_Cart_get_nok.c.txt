     1	// DO NOT EDIT: this file was generated by /MBI/scripts/CollTopoGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: The code tries to get cartesian information of MPI_COMM_WORLD.
     7	    The code creates a cartesian communicator, and tries to get cartesian information of MPI_COMM_WORLD.
     8	
     9		 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Lacking
    16		COLL!nonblocking: Lacking
    17		COLL!persistent: Lacking
    18		COLL!tools: Yes
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: InvalidCommunicator
    25	  | Invalid Communicator in a collective. MPI_Cart_get at CollInvalidCom_Cart_get_nok.c:62 tries to get cartesian information of MPI_COMM_WORLD.
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	#define buff_size 128
    34	
    35	int main(int argc, char **argv) {
    36	  int nprocs = -1;
    37	  int rank = -1;
    38	
    39	  MPI_Init(&argc, &argv);
    40	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    41	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    42	  printf("Hello from rank %d \n", rank);
    43	
    44	  if (nprocs < 2)
    45	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    46	
    47		 MPI_Comm newcom;
    48	   int dims[2], periods[2], coords[2];
    49	   int source, dest;
    50	   dims[0] = 2;
    51	   dims[1] = 1;
    52	   periods[0] = 1;
    53	   periods[1] = 1;
    54	 
    55	   /* No error injected here */
    56	
    57	   MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &newcom); /* create a cartesian communicator */
    58	
    59	   newcom = MPI_COMM_WORLD; /* MBIERROR1 */
    60	
    61	   
    62	   MPI_Cart_get(newcom, 2, dims, periods, coords); /* MBIERROR2 */
    63		 
    64	 
    65	   if (newcom != MPI_COMM_NULL)
    66	     MPI_Comm_free(&newcom);
    67	
    68	  MPI_Finalize();
    69	  printf("Rank %d finished normally\n", rank);
    70	  return 0;
    71	}
