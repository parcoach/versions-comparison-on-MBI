Compiling CollCorrect_Ialltoallv_Iscatter.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Ialltoallv_Iscatter.c -o CollCorrect_Ialltoallv_Iscatter.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ialltoallv_Iscatter.bc -o /dev/null
missing info for external function MPI_Ialltoallv
missing info for external function MPI_Iscatter
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04114484786987305
