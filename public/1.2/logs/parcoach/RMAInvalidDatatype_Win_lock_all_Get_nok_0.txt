Compiling RMAInvalidDatatype_Win_lock_all_Get_nok.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -o RMAInvalidDatatype_Win_lock_all_Get_nok.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi RMAInvalidDatatype_Win_lock_all_Get_nok.bc -o /dev/null
missing info for external function MPI_Win_lock_all
missing info for external function MPI_Win_unlock_all
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.03924965858459473
