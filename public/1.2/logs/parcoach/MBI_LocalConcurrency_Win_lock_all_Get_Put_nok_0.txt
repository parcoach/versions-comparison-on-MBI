Compiling MBI_LocalConcurrency_Win_lock_all_Get_Put_nok.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_Get_Put_nok.c -o MBI_LocalConcurrency_Win_lock_all_Get_Put_nok.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi MBI_LocalConcurrency_Win_lock_all_Get_Put_nok.bc -o /dev/null
missing info for external function MPI_Win_lock_all
missing info for external function MPI_Win_unlock_all
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04172706604003906
