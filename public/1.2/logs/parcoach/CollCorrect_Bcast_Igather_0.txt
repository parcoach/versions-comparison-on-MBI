Compiling CollCorrect_Bcast_Igather.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Bcast_Igather.c -o CollCorrect_Bcast_Igather.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Bcast_Igather.bc -o /dev/null
missing info for external function MPI_Igather
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.040155649185180664
