Compiling CollCorrect_Barrier_Iallgatherv.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Barrier_Iallgatherv.c -o CollCorrect_Barrier_Iallgatherv.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Barrier_Iallgatherv.bc -o /dev/null
missing info for external function MPI_Iallgatherv
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04096078872680664
