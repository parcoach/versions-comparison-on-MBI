Compiling GlobalConcurrency_Win_fence_Put_Get_nok.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -o GlobalConcurrency_Win_fence_Put_Get_nok.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi GlobalConcurrency_Win_fence_Put_Get_nok.bc -o /dev/null
* AA done
* PTA Call graph creation done
15 regions
0 regions created (0.000000e+00%)
* Regions creation done
* Mod/ref done
MSSA: visited 0 functions over 12 (0.000000e+00%)
* SSA done
DepGraph: visited 0 functions over 12 (0.000000e+00%)
* Dep graph done
for each call site get PDF+ and save tainted conditions
* value contamination  done
* Starting Parcoach analysis ...
 (1) BFS
 (2) CheckCollectives
 ... Parcoach analysis done
==========================================
===  PARCOACH INTER WITH DEP ANALYSIS  ===
==========================================
Module name: GlobalConcurrency_Win_fence_Put_Get_nok.bc
0 collective(s) found
0 collective(s) conditionally called
0 warning(s) issued
0 cond(s) 
0 different cond(s)
0 CC functions inserted 
0 condition(s) added and 0 condition(s) removed with dep analysis.
0 warning(s) added and 0 warning(s) removed with dep analysis.
==========================================
Stack dump:
0.	Program arguments: opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi GlobalConcurrency_Win_fence_Put_Get_nok.bc -o /dev/null 
Command killed by signal 11, elapsed time: 0.047916412353515625
