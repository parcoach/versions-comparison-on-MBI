Compiling CollCorrect_Iallgatherv_Ireduce.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Iallgatherv_Ireduce.c -o CollCorrect_Iallgatherv_Ireduce.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Iallgatherv_Ireduce.bc -o /dev/null
missing info for external function MPI_Iallgatherv
missing info for external function MPI_Ireduce
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04113960266113281
