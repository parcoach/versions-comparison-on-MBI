Compiling CollCallOrder_Bcast_none_nok.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCallOrder_Bcast_none_nok.c -o CollCallOrder_Bcast_none_nok.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCallOrder_Bcast_none_nok.bc -o /dev/null
* AA done
* PTA Call graph creation done
16 regions
0 regions created (0.000000e+00%)
* Regions creation done
* Mod/ref done
MSSA: visited 0 functions over 9 (0.000000e+00%)
* SSA done
DepGraph: visited 0 functions over 9 (0.000000e+00%)
* Dep graph done
for each call site get PDF+ and save tainted conditions
* value contamination  done
* Starting Parcoach analysis ...
 (1) BFS
 (2) CheckCollectives
PARCOACH: gencodes/CollCallOrder_Bcast_none_nok.c: warning: MPI_Bcast line 57 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCallOrder_Bcast_none_nok.c) (full-inter)
 ... Parcoach analysis done
==========================================
===  PARCOACH INTER WITH DEP ANALYSIS  ===
==========================================
Module name: CollCallOrder_Bcast_none_nok.bc
1 collective(s) found
1 collective(s) conditionally called
1 warning(s) issued
1 cond(s) 
1 different cond(s)
0 CC functions inserted 
0 condition(s) added and 0 condition(s) removed with dep analysis.
0 warning(s) added and 0 warning(s) removed with dep analysis.
==========================================
Stack dump:
0.	Program arguments: opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCallOrder_Bcast_none_nok.bc -o /dev/null 
Command killed by signal 11, elapsed time: 0.04758954048156738
