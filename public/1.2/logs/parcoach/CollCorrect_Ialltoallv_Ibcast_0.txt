Compiling CollCorrect_Ialltoallv_Ibcast.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Ialltoallv_Ibcast.c -o CollCorrect_Ialltoallv_Ibcast.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ialltoallv_Ibcast.bc -o /dev/null
missing info for external function MPI_Ialltoallv
missing info for external function MPI_Ibcast
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.0414731502532959
