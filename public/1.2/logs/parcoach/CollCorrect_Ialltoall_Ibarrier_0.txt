Compiling CollCorrect_Ialltoall_Ibarrier.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Ialltoall_Ibarrier.c -o CollCorrect_Ialltoall_Ibarrier.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ialltoall_Ibarrier.bc -o /dev/null
missing info for external function MPI_Ialltoall
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04102826118469238
