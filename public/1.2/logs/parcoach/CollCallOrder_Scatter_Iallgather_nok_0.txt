Compiling CollCallOrder_Scatter_Iallgather_nok.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCallOrder_Scatter_Iallgather_nok.c -o CollCallOrder_Scatter_Iallgather_nok.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCallOrder_Scatter_Iallgather_nok.bc -o /dev/null
missing info for external function MPI_Iallgather
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04126238822937012
