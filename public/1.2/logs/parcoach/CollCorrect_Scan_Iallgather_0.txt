Compiling CollCorrect_Scan_Iallgather.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Scan_Iallgather.c -o CollCorrect_Scan_Iallgather.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Scan_Iallgather.bc -o /dev/null
missing info for external function MPI_Iallgather
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04019618034362793
