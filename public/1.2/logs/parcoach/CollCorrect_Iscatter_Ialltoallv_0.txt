Compiling CollCorrect_Iscatter_Ialltoallv.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Iscatter_Ialltoallv.c -o CollCorrect_Iscatter_Ialltoallv.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Iscatter_Ialltoallv.bc -o /dev/null
missing info for external function MPI_Iscatter
missing info for external function MPI_Ialltoallv
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.04254794120788574
