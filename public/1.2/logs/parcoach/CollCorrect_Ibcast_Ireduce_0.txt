Compiling CollCorrect_Ibcast_Ireduce.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Ibcast_Ireduce.c -o CollCorrect_Ibcast_Ireduce.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ibcast_Ireduce.bc -o /dev/null
missing info for external function MPI_Ibcast
missing info for external function MPI_Ireduce
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.041397809982299805
