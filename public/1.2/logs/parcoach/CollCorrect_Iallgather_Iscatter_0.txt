Compiling CollCorrect_Iallgather_Iscatter.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Iallgather_Iscatter.c -o CollCorrect_Iallgather_Iscatter.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Iallgather_Iscatter.bc -o /dev/null
missing info for external function MPI_Iallgather
missing info for external function MPI_Iscatter
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.039609670639038086
