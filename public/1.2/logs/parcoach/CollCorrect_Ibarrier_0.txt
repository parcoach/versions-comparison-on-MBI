Compiling CollCorrect_Ibarrier.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Ibarrier.c -o CollCorrect_Ibarrier.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ibarrier.bc -o /dev/null
* AA done
* PTA Call graph creation done
15 regions
0 regions created (0.000000e+00%)
* Regions creation done
* Mod/ref done
MSSA: visited 0 functions over 10 (0.000000e+00%)
* SSA done
DepGraph: visited 0 functions over 10 (0.000000e+00%)
* Dep graph done
for each call site get PDF+ and save tainted conditions
* value contamination  done
* Starting Parcoach analysis ...
 (1) BFS
 (2) CheckCollectives
PARCOACH: gencodes/CollCorrect_Ibarrier.c: warning: MPI_Ibarrier line 57 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Ibarrier.c) (full-inter)
PARCOACH: gencodes/CollCorrect_Ibarrier.c: warning: MPI_Ibarrier line 62 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Ibarrier.c) (full-inter)
 ... Parcoach analysis done
==========================================
===  PARCOACH INTER WITH DEP ANALYSIS  ===
==========================================
Module name: CollCorrect_Ibarrier.bc
2 collective(s) found
2 collective(s) conditionally called
2 warning(s) issued
2 cond(s) 
1 different cond(s)
0 CC functions inserted 
0 condition(s) added and 0 condition(s) removed with dep analysis.
0 warning(s) added and 0 warning(s) removed with dep analysis.
==========================================
Stack dump:
0.	Program arguments: opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Ibarrier.bc -o /dev/null 
Command killed by signal 11, elapsed time: 0.04704594612121582
