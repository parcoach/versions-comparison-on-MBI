Compiling CollCorrect_Allreduce_Ialltoall.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Allreduce_Ialltoall.c -o CollCorrect_Allreduce_Ialltoall.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Allreduce_Ialltoall.bc -o /dev/null
missing info for external function MPI_Ialltoall
Error: you have to fill the funcModPairs array in ExtInfo.cpp with the missing functions. exiting..
Command return code: 1, elapsed time: 0.041152238845825195
