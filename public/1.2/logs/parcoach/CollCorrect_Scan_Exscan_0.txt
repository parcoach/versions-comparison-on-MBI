Compiling CollCorrect_Scan_Exscan.c (batchinfo:1/1)

$ MPICH_CC=clang-9 mpicc -c -g -emit-llvm /MBI/gencodes/CollCorrect_Scan_Exscan.c -o CollCorrect_Scan_Exscan.bc


Executing the command
 $ opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Scan_Exscan.bc -o /dev/null
* AA done
* PTA Call graph creation done
17 regions
0 regions created (0.000000e+00%)
* Regions creation done
* Mod/ref done
MSSA: visited 0 functions over 9 (0.000000e+00%)
* SSA done
DepGraph: visited 0 functions over 9 (0.000000e+00%)
* Dep graph done
for each call site get PDF+ and save tainted conditions
* value contamination  done
* Starting Parcoach analysis ...
 (1) BFS
 (2) CheckCollectives
PARCOACH: gencodes/CollCorrect_Scan_Exscan.c: warning: MPI_Scan line 57 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Scan_Exscan.c) (full-inter)
PARCOACH: gencodes/CollCorrect_Scan_Exscan.c: warning: MPI_Exscan line 59 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Scan_Exscan.c) (full-inter)
PARCOACH: gencodes/CollCorrect_Scan_Exscan.c: warning: MPI_Scan line 62 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Scan_Exscan.c) (full-inter)
PARCOACH: gencodes/CollCorrect_Scan_Exscan.c: warning: MPI_Exscan line 64 possibly not called by all processes because of conditional(s) line(s)  56 (gencodes/CollCorrect_Scan_Exscan.c) (full-inter)
 ... Parcoach analysis done
==========================================
===  PARCOACH INTER WITH DEP ANALYSIS  ===
==========================================
Module name: CollCorrect_Scan_Exscan.bc
4 collective(s) found
4 collective(s) conditionally called
4 warning(s) issued
4 cond(s) 
1 different cond(s)
0 CC functions inserted 
0 condition(s) added and 0 condition(s) removed with dep analysis.
0 warning(s) added and 0 warning(s) removed with dep analysis.
==========================================
Stack dump:
0.	Program arguments: opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi CollCorrect_Scan_Exscan.bc -o /dev/null 
Command killed by signal 11, elapsed time: 0.04710745811462402
