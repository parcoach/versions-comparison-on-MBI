     1	// DO NOT EDIT: this file was generated by /MBI/scripts/ResleakGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: MPI_Op_create has no free
     7	    MPI_Op_create has no free
     8	
     9		Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Lacking
    16		COLL!nonblocking: Lacking 
    17		COLL!persistent: Lacking
    18		COLL!tools: Yes
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: OperatorLeak
    25	  | Resleak. MPI_Op_create at Resleak_Op_create_nok.c:60 has no free.
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	
    32	#define ITERATIONS 100
    33	#define PARAM_PER_ITERATION 3
    34	#define PARAM_LOST_PER_ITERATION 1
    35	
    36	void myOp(int *invec, int *inoutvec, int *len, MPI_Datatype *dtype) {
    37	  for (int i = 0; i < *len; i++)
    38	    inoutvec[i] += invec[i];
    39	}
    40	
    41	int main(int argc, char **argv) {
    42	  int nprocs = -1;
    43	  int rank = -1;
    44		int i=1, j=1, size=1;
    45		int color =0;
    46	
    47	  MPI_Init(&argc, &argv);
    48	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    49	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    50	  printf("Hello from rank %d \n", rank);
    51	
    52	  if (nprocs < 2)
    53	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    54	
    55		
    56	  MPI_Op op[size];
    57			
    58	  MPI_Op_create((MPI_User_function *)myOp, 0, &op[j]); 
    59			
    60		 /* MBIERROR MISSING: MPI_Op_free(&op[j]); */
    61			
    62	
    63		
    64	
    65	  MPI_Finalize();
    66	  printf("Rank %d finished normally\n", rank);
    67	  return 0;
    68	}
