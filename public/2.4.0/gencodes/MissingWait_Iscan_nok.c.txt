     1	// DO NOT EDIT: this file was generated by /MBI/scripts/MissingWaitandStartGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: Missing wait
     7	    Missing Wait. MPI_Iscan at MissingWait_Iscan_nok.c:61 has no completion
     8		
     9		 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Lacking
    16		COLL!nonblocking: Yes
    17		COLL!persistent: Lacking
    18		COLL!tools: Lacking
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: MissingWait
    25	  | ERROR: MissingWait
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	
    34	int main(int argc, char **argv) {
    35	  int nprocs = -1;
    36	  int rank = -1;
    37		int root = 0;
    38	
    39	  MPI_Init(&argc, &argv);
    40	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    41	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    42	  printf("Hello from rank %d \n", rank);
    43	
    44	  if (nprocs < 2)
    45	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    46	
    47		MPI_Comm newcom = MPI_COMM_WORLD;
    48		MPI_Datatype type = MPI_INT;
    49	  MPI_Op op = MPI_SUM;
    50		int stag = 0, rtag = 0;
    51	  int buff_size = 1;
    52	
    53		int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
    54	
    55	  int dest = (rank == nprocs - 1) ? (0) : (rank + 1);
    56	  int src = (rank == 0) ? (nprocs - 1) : (rank - 1); 
    57	
    58	  MPI_Request req1=MPI_REQUEST_NULL;MPI_Status sta1; int outbuf1[buff_size], inbuf1[buff_size];
    59	  
    60	
    61	  MPI_Iscan(&outbuf1, inbuf1, buff_size, type, op, newcom,&req1); /* MBIERROR */ 
    62	  
    63	   
    64	  
    65	
    66		 /* MBIERROR MISSING: MPI_Wait(&req1,&sta1); */ 
    67		  
    68	
    69		 /* MISSING: if(req1 != MPI_REQUEST_NULL) MPI_Request_free(&req1); (to not free the buffer before an internal wait */
    70		
    71	
    72	  MPI_Finalize();
    73	  printf("Rank %d finished normally\n", rank);
    74	  return 0;
    75	}
