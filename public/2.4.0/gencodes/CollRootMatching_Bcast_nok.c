// DO NOT EDIT: this file was generated by /MBI/scripts/CollArgGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Collective MPI_Bcast with a root mismatch
    Odd ranks use 0 as a root while even ranks use 1 as a root

	Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation

BEGIN_MPI_FEATURES
	P2P!basic: Lacking
	P2P!nonblocking: Lacking
	P2P!persistent: Lacking
	COLL!basic: Yes
	COLL!nonblocking: Lacking
	COLL!persistent: Lacking
	COLL!tools: Lacking  
	RMA: Lacking
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | ERROR: RootMatching
  | Collective root mistmatch. MPI_Bcast at CollRootMatching_Bcast_nok.c:60 has 0 or 1 as a root.
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define buff_size 128

int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
	int root = 0;
	int size = 1, j=0, color=0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");

	MPI_Comm newcom = MPI_COMM_WORLD;
	MPI_Op op = MPI_SUM;
	MPI_Datatype type = MPI_INT;

	int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */  

  int buf1[buff_size];
  

  if (rank % 2)
		root = 1; /* MBIERROR1 */
  MPI_Bcast(buf1, buff_size, type, root, newcom); /* MBIERROR2 */  
	
	

  MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
