     1	// DO NOT EDIT: this file was generated by /MBI/scripts/CollMatchingGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description: Correct collective ordering
     7	    All ranks call MPI_Allgatherv and then MPI_Allgather
     8	
     9		 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking
    13		P2P!nonblocking: Lacking
    14		P2P!persistent: Lacking
    15		COLL!basic: Yes
    16		COLL!nonblocking: Lacking
    17		COLL!persistent: Lacking
    18		COLL!tools: Lacking
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | OK
    25	  | 
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	#define buff_size 128
    34	
    35	int main(int argc, char **argv) {
    36	  int nprocs = -1;
    37	  int rank = -1;
    38		int root = 0;
    39	
    40	  MPI_Init(&argc, &argv);
    41	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    42	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    43	  printf("Hello from rank %d \n", rank);
    44	
    45	  if (nprocs < 2)
    46	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug.\n");
    47	
    48		MPI_Comm newcom = MPI_COMM_WORLD;
    49		MPI_Datatype type = MPI_INT;
    50	  MPI_Op op = MPI_SUM;
    51	
    52	  int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
    53	  int *rbuf1 = (int*)malloc(dbs*2), *rcounts1=(int*)malloc(dbs),  *displs1=(int*)malloc(dbs);
    54	  for (int i = 0; i < nprocs; i++) {
    55	    rcounts1[i] = 1;
    56	    displs1[i] = 2 * (nprocs - (i + 1));
    57	  }
    58	  int val2=1, *rbuf2 = (int*)malloc(dbs);
    59	
    60	  if (rank % 2) {
    61	    MPI_Allgatherv(&rank, 1, type, rbuf1, rcounts1, displs1, type, newcom); /* MBIERROR1 */
    62	  	
    63	    MPI_Allgather(&val2, 1, type, rbuf2, 1, type, newcom);
    64	  	
    65	  } else {
    66	    MPI_Allgatherv(&rank, 1, type, rbuf1, rcounts1, displs1, type, newcom); /* MBIERROR2 */
    67	  	
    68	    MPI_Allgather(&val2, 1, type, rbuf2, 1, type, newcom);
    69	  	
    70	  }
    71	
    72	  free(rbuf1);free(rcounts1);free(displs1);
    73	  free(rbuf2);
    74	  
    75	  MPI_Finalize();
    76	  printf("Rank %d finished normally\n", rank);
    77	  return 0;
    78	}
