// DO NOT EDIT: this file was generated by /MBI/scripts/CollMatchingGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Correct collective ordering
    All ranks call MPI_Ialltoall and then MPI_Allreduce

	 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation

BEGIN_MPI_FEATURES
	P2P!basic: Lacking
	P2P!nonblocking: Lacking
	P2P!persistent: Lacking
	COLL!basic: Yes
	COLL!nonblocking: Yes
	COLL!persistent: Lacking
	COLL!tools: Lacking
	RMA: Lacking
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | OK
  | 
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

#define buff_size 128

int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
	int root = 0;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug.\n");

	MPI_Comm newcom = MPI_COMM_WORLD;
	MPI_Datatype type = MPI_INT;
  MPI_Op op = MPI_SUM;

  int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
  MPI_Request req1=MPI_REQUEST_NULL;MPI_Status sta1;int *sbuf1 = (int*)malloc(dbs), *rbuf1 = (int*)malloc(dbs);
  int sum2, val2 = 1;

  if (rank % 2) {
    MPI_Ialltoall(sbuf1, 1, type, rbuf1, 1, type, newcom, &req1); /* MBIERROR1 */
  	MPI_Wait(&req1,&sta1);
    MPI_Allreduce(&val2, &sum2, 1, type, op, newcom);
  	
  } else {
    MPI_Ialltoall(sbuf1, 1, type, rbuf1, 1, type, newcom, &req1); /* MBIERROR2 */
  	MPI_Wait(&req1,&sta1);
    MPI_Allreduce(&val2, &sum2, 1, type, op, newcom);
  	
  }

  free(sbuf1);free(rbuf1);
  
  
  MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
