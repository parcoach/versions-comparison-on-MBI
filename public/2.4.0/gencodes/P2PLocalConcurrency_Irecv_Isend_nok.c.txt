     1	// DO NOT EDIT: this file was generated by /MBI/scripts/P2PLocalConcurrencyGenerator.py. DO NOT EDIT.
     2	/* ///////////////////////// The MPI Bugs Initiative ////////////////////////
     3	
     4	  Origin: MBI
     5	
     6	  Description:  Local Concurrency with a P2P
     7	    The message buffer in MPI_Isend and MPI_Irecv are modified before the calls have completed.
     8	
     9		 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation
    10	
    11	BEGIN_MPI_FEATURES
    12		P2P!basic: Lacking 
    13		P2P!nonblocking: Yes
    14		P2P!persistent: Lacking
    15		COLL!basic: Lacking
    16		COLL!nonblocking: Lacking
    17		COLL!persistent: Lacking
    18		COLL!tools: Lacking
    19		RMA: Lacking
    20	END_MPI_FEATURES
    21	
    22	BEGIN_MBI_TESTS
    23	  $ mpirun -np 2 ${EXE}
    24	  | ERROR: LocalConcurrency
    25	  | Local Concurrency with a P2P. The message buffers in MPI_Isend and MPI_Irecv are modified at P2PLocalConcurrency_Irecv_Isend_nok.c:58 and P2PLocalConcurrency_Irecv_Isend_nok.c:64 whereas there is no guarantee the calls have been completed.
    26	END_MBI_TESTS
    27	//////////////////////       End of MBI headers        /////////////////// */
    28	
    29	#include <mpi.h>
    30	#include <stdio.h>
    31	#include <stdlib.h>
    32	
    33	
    34	int main(int argc, char **argv) {
    35	  int nprocs = -1;
    36	  int rank = -1;
    37		int dest=0, src=0;
    38		int stag = 0, rtag = 0;
    39	  int buff_size = 1;
    40	
    41	  MPI_Init(&argc, &argv);
    42	  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
    43	  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    44	  printf("Hello from rank %d \n", rank);
    45	
    46	  if (nprocs < 2)
    47	    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");
    48	
    49	  MPI_Comm newcom = MPI_COMM_WORLD;
    50		MPI_Datatype type = MPI_INT;
    51	
    52	  int buf1=rank; MPI_Request req1=MPI_REQUEST_NULL;
    53	  int buf2=-1; MPI_Request req2=MPI_REQUEST_NULL;
    54		if (rank == 0) {
    55			dest = 1; src = 1;
    56	  	MPI_Isend(&buf1, buff_size, type, dest, stag, newcom, &req1); 
    57			
    58			buf1=4; /* MBIERROR1 */ 
    59			MPI_Wait(&req1, MPI_STATUS_IGNORE);
    60		}else if (rank == 1){
    61			dest = 0; src = 0;
    62	  	MPI_Irecv(&buf2, buff_size, type, src, rtag, newcom, &req2);
    63			
    64			buf2++; /* MBIERROR2 */
    65			 MPI_Wait(&req2, MPI_STATUS_IGNORE);
    66		}
    67	
    68	  MPI_Finalize();
    69	  printf("Rank %d finished normally\n", rank);
    70	  return 0;
    71	}
