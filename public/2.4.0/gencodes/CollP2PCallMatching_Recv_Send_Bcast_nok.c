// DO NOT EDIT: this file was generated by /MBI/scripts/CollP2PMatchingGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Point to point & collective mismatch
    Point to point MPI_Recv is matched with MPI_Bcast which causes a deadlock.

	 Version of MPI: Conforms to MPI 1.1, does not require MPI 2 implementation

BEGIN_MPI_FEATURES
	P2P!basic: Yes 
	P2P!nonblocking: Lacking
	P2P!persistent: Lacking
	COLL!basic: Yes 
	COLL!nonblocking: Lacking
	COLL!persistent: Lacking
	COLL!tools: Lacking
	RMA: Lacking
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | ERROR: CallMatching
  | P2P & Collective mistmatch. MPI_Recv at CollP2PCallMatching_Recv_Send_Bcast_nok.c:66 is matched with MPI_Bcast at CollP2PCallMatching_Recv_Send_Bcast_nok.c:60 wich causes a deadlock.
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
  int dest, src;
  int root = 0;
	int stag = 0, rtag = 0;
	int buff_size = 1;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");

  int dbs = sizeof(int)*nprocs; /* Size of the dynamic buffers for alltoall and friends */
	MPI_Comm newcom = MPI_COMM_WORLD;
	MPI_Datatype type = MPI_INT;
	MPI_Op op = MPI_SUM;  

  int buf1=rank;
  int buf2=-1; MPI_Status sta2;
  int buf3[buff_size];
	if (rank == 0) {
		dest=1;src=1;
  	MPI_Bcast(buf3, buff_size, type, root, newcom); /* MBIERROR1 */
		
  	MPI_Send(&buf1, buff_size, type, dest, stag, newcom); 
		
	}else if (rank==1) {
		dest=0;src=0;
  	MPI_Recv(&buf2, buff_size, type, src, rtag, newcom, &sta2); /* MBIERROR2 */
		
  	MPI_Bcast(buf3, buff_size, type, root, newcom); 
		
	}

	
	
	
  
	MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
