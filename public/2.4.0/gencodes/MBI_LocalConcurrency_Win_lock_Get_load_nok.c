// DO NOT EDIT: this file was generated by /MBI/scripts/RMALocalConcurrencyGenerator.py. DO NOT EDIT.
/* ///////////////////////// The MPI Bugs Initiative ////////////////////////

  Origin: MBI

  Description: Local Concurrency error.
    Local Concurrency error. load conflicts with MPI_Get

	Version of MPI: Conforms to MPI 2, requires MPI 3 implementation (for lock_all/unlock_all epochs)

BEGIN_MPI_FEATURES
	P2P!basic: Lacking 
	P2P!nonblocking: Lacking
	P2P!persistent: Lacking
	COLL!basic: Lacking
	COLL!nonblocking: Lacking
	COLL!persistent: Lacking
	COLL!tools: Lacking
	RMA: Yes
END_MPI_FEATURES

BEGIN_MBI_TESTS
  $ mpirun -np 2 ${EXE}
  | ERROR: LocalConcurrency
  | Local Concurrency error. load at MBI_LocalConcurrency_Win_lock_Get_load_nok.c:62 conflicts with MPI_Get line 61
END_MBI_TESTS
//////////////////////       End of MBI headers        /////////////////// */

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char **argv) {
  int nprocs = -1;
  int rank = -1;
	MPI_Win win;
  int W; // Window buffer
	int NUM_ELEMT=1;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  printf("Hello from rank %d \n", rank);

  if (nprocs < 2)
    printf("MBI ERROR: This test needs at least 2 processes to produce a bug!\n");

	MPI_Datatype type = MPI_INT;

	int target = (rank + 1) % nprocs;
  W = 4;

  MPI_Win_create(&W, NUM_ELEMT * sizeof(int), sizeof(int), MPI_INFO_NULL, MPI_COMM_WORLD, &win);

  MPI_Win_lock(MPI_LOCK_SHARED, target, 0, win);
  
	int localbuf1 = 10; 

	if (rank == 0) {
  	MPI_Get(&localbuf1, NUM_ELEMT, MPI_INT, target, 0, NUM_ELEMT, type, win); /* MBIERROR1 */
  	int load = localbuf1 % 2; /* MBIERROR2 */
	}

  MPI_Win_unlock(target, win);

  MPI_Win_free(&win);

  MPI_Finalize();
  printf("Rank %d finished normally\n", rank);
  return 0;
}
