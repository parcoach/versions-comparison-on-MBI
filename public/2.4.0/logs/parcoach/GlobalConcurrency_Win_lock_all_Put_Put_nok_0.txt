Compiling GlobalConcurrency_Win_lock_all_Put_Put_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -g -S -emit-llvm -o parcoach-ir-17c45f.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-17c45f.ll -o parcoach-ir-42a6af.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 0 MPI_Get, 2 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/18) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-42a6af.ll -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_all_Put_Put_nok.c -g -S -emit-llvm -o parcoach-ir-57406b.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-57406b.ll -o parcoach-ir-7e12c2.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 0 MPI_Get, 2 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/18) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-7e12c2.ll -c -o GlobalConcurrency_Win_lock_all_Put_Put_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.1559767723083496
