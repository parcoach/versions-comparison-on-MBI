Compiling MBI_LocalConcurrency_Win_lock_Get_store_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -g -S -emit-llvm -o parcoach-ir-471d41.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-471d41.ll -o parcoach-ir-7009ab.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  store i32 8, ptr %13, align 4, !dbg !102 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !101 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 9 (/16) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-7009ab.ll -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c -g -S -emit-llvm -o parcoach-ir-cb2aa3.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-cb2aa3.ll -o parcoach-ir-96144b.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  store i32 8, ptr %13, align 4, !dbg !102 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !101 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_lock_Get_store_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 9 (/16) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-96144b.ll -c -o MBI_LocalConcurrency_Win_lock_Get_store_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.1480085849761963
