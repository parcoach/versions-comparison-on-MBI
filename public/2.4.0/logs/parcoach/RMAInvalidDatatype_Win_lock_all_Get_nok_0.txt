Compiling RMAInvalidDatatype_Win_lock_all_Get_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -g -S -emit-llvm -o parcoach-ir-897436.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-897436.ll -o parcoach-ir-c35757.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
12 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 5 (/13) LOAD and 1 (/10) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-c35757.ll -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/RMAInvalidDatatype_Win_lock_all_Get_nok.c -g -S -emit-llvm -o parcoach-ir-59beff.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-59beff.ll -o parcoach-ir-e5e1ed.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
12 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 5 (/13) LOAD and 1 (/10) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-e5e1ed.ll -c -o RMAInvalidDatatype_Win_lock_all_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.15157485008239746
