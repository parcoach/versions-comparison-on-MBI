Compiling MBI_LocalConcurrency_Win_fence_Get_load_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -g -S -emit-llvm -o parcoach-ir-90b597.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-90b597.ll -o parcoach-ir-975a3e.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  %44 = load i32, ptr %13, align 4, !dbg !103 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !100 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 2 MPI_Win_fence, 0 MPI_Lock, 0 MPI_Lockall 0 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 9 (/15) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-975a3e.ll -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c -g -S -emit-llvm -o parcoach-ir-172799.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-172799.ll -o parcoach-ir-2317c2.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  %44 = load i32, ptr %13, align 4, !dbg !103 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !100 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_fence_Get_load_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 2 MPI_Win_fence, 0 MPI_Lock, 0 MPI_Lockall 0 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 9 (/15) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-2317c2.ll -c -o MBI_LocalConcurrency_Win_fence_Get_load_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.1509997844696045
