Compiling MBI_LocalConcurrency_Win_lock_Get_Get_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -g -S -emit-llvm -o parcoach-ir-47b6cd.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-47b6cd.ll -o parcoach-ir-ae037b.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  %49 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %44, ptr noundef @ompi_mpi_int, i32 noundef %45, i64 noundef 0, i32 noundef %46, ptr noundef %47, ptr noundef %48), !dbg !107 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !101 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 2 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/21) LOAD and 1 (/10) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-ae037b.ll -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c -g -S -emit-llvm -o parcoach-ir-bd8da2.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-bd8da2.ll -o parcoach-ir-0c8a69.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...LocalConcurrency detected: conflit with the following instructions: 
  %49 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %44, ptr noundef @ompi_mpi_int, i32 noundef %45, i64 noundef 0, i32 noundef %46, ptr noundef %47, ptr noundef %48), !dbg !107 - LINE 62 in gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c
AND
  %43 = call i32 @MPI_Get(ptr noundef %13, i32 noundef %38, ptr noundef @ompi_mpi_int, i32 noundef %39, i64 noundef 0, i32 noundef %40, ptr noundef %41, ptr noundef %42), !dbg !101 - LINE 61 in gencodes/MBI_LocalConcurrency_Win_lock_Get_Get_nok.c
done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 2 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/21) LOAD and 1 (/10) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-0c8a69.ll -c -o MBI_LocalConcurrency_Win_lock_Get_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.15005016326904297
