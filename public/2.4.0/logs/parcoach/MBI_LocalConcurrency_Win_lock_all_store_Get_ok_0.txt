Compiling MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -g -S -emit-llvm -o parcoach-ir-d9f7d8.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-d9f7d8.ll -o parcoach-ir-714f92.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 8 (/14) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-714f92.ll -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/MBI_LocalConcurrency_Win_lock_all_store_Get_ok.c -g -S -emit-llvm -o parcoach-ir-2b0985.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-2b0985.ll -o parcoach-ir-3936d7.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
9 MPI functions including 5 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 0 MPI_Lock, 1 MPI_Lockall 0 MPI_Unlock, 1 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 0 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 8 (/14) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-3936d7.ll -c -o MBI_LocalConcurrency_Win_lock_all_store_Get_ok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.1447911262512207
