Compiling GlobalConcurrency_Win_fence_Put_Get_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -g -S -emit-llvm -o parcoach-ir-75e819.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-75e819.ll -o parcoach-ir-ba365d.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 2 MPI_Win_fence, 0 MPI_Lock, 0 MPI_Lockall 0 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 1 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/18) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-ba365d.ll -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_fence_Put_Get_nok.c -g -S -emit-llvm -o parcoach-ir-25ade9.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-25ade9.ll -o parcoach-ir-74e12a.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 2 MPI_Win_fence, 0 MPI_Lock, 0 MPI_Lockall 0 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 1 MPI_Get, 1 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 14 (/18) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-74e12a.ll -c -o GlobalConcurrency_Win_fence_Put_Get_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.14895415306091309
