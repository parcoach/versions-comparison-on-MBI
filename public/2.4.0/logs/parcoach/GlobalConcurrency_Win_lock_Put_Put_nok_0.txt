Compiling GlobalConcurrency_Win_lock_Put_Put_nok.c (batchinfo:1/1)

$ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -g -S -emit-llvm -o parcoach-ir-350851.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-350851.ll -o parcoach-ir-ec8d3a.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 0 MPI_Get, 2 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 15 (/20) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-ec8d3a.ll -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]


Executing the command
 $ parcoachcc -check=rma --args mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o'
remark: Parcoach: running '/usr/bin/mpicc /MBI/gencodes/GlobalConcurrency_Win_lock_Put_Put_nok.c -g -S -emit-llvm -o parcoach-ir-63e228.ll'
remark: Parcoach: running '/MBI/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/parcoach -check=rma parcoach-ir-63e228.ll -o parcoach-ir-d55900.ll'
===========================
===  PARCOACH ANALYSIS  ===
===========================
===========================
ANALYZING function main...
(1) Get statistics ...done 
(2) Local concurrency errors detection ...done 
(3) Instrumentation for dynamic analysis ...done 
=== STATISTICS === 
10 MPI functions including 6 RMA functions 
= WINDOW CREATION/DESTRUCTION: 1 MPI_Win_free, 1 MPI_Win_create 
= EPOCH CREATION/DESTRUCTION: 0 MPI_Win_fence, 1 MPI_Lock, 0 MPI_Lockall 1 MPI_Unlock, 0 MPI_Unlockall 
= ONE-SIDED COMMUNICATIONS: 0 MPI_Get, 2 MPI_Put, 0 MPI_Accumulate 
= SYNCHRONIZATION: 0 MPI_Win_Flush 
LOAD/STORE STATISTICS: 15 (/20) LOAD and 2 (/11) STORE are instrumented
===========================
remark: Parcoach: running '/usr/bin/mpicc parcoach-ir-d55900.ll -c -o GlobalConcurrency_Win_lock_Put_Put_nok.o'
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include' [-Wunused-command-line-argument]
clang: warning: argument unused during compilation: '-I /usr/lib/x86_64-linux-gnu/openmpi/include/openmpi' [-Wunused-command-line-argument]
Command return code: 0, elapsed time: 0.15290188789367676
