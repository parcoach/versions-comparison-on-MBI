# The MPI Bugs Initiative

The MPI Bugs Initiative is a collection of MPI codes that aims at assessing the status of MPI verification tools.

For a gentle introduction, please refer to the [corresponding article](https://hal.archives-ouvertes.fr/hal-03474762).

## Quick Start

Docker is used to facilitate the benchmark modifications and the installation of the tools we have selected.
You can create a docker image containing all dependencies needed to evaluate the tools using the provided Dockerfile.
The following commands will create and run the image:
```bash
docker build -f Dockerfile -t mbi:latest .
docker run -it mbi bash 
```

Once inside the docker, the scripts you need are in /MBI/scripts. One script is provided for each tool. /MBI/scripts/{tool}-build builds and installs the selected tool.
The result can then be explored under /MBI/logs/{tool} (a new such directory is created each time you launch a tool)). 
Three files are created per test: 
- {test_name}.txt that contains the output of the test 
- {test_name}.elapsed that gives the time of the test
- {test_name}.md5sum which is the cache

A test is launched if {test_name}.txt or {test_name}.elapsed do not exist or if {test_name} has been modified ({test_name}.md5sum has changed).

Command to generate all c codes:
```bash
python3 MBI.py -c generate
```

You can launch all tests outside the docker image by using
```bash
./test-all
```

To test a specific tool:
1. Build the docker container: 
```bash 
docker build -f Dockerfile -t mbi:latest .
 ```
2. Run the container: 
```bash
docker run -it mbi bash
 ```
3. Build and run a tool: 
```bash
python3 ./MBI/MBI.py -x (tool) -c run 
```
4. Get statistics on a tool:
```bash
python3 ./MBI/MBI.py -x (tool) -c stats 
```
