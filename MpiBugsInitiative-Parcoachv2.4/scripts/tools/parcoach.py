import re
import os
from MBIutils import *

class Tool(AbstractTool):
    def identify(self):
        return "PARCOACH wrapper"

    def ensure_image(self):
        AbstractTool.ensure_image(self, "-x parcoach")

    def build(self, rootdir, cached=True):
        #if cached and os.path.exists(f"{rootdir}/builds/parcoach/src/aSSA/aSSA.so"):
        if cached and os.path.exists(f"{rootdir}/builds/parcoach/bin/parcoachcc"):
            print("No need to rebuild PARCOACH.")
            os.environ['PATH'] = os.environ['PATH'] + f":/{rootdir}/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/"
            os.environ['OMPI_CC'] = "clang-15"
            return

        here = os.getcwd() # Save where we were
        os.chdir(rootdir)
        # Get a GIT checkout. Either create it, or refresh it
        #if os.path.exists("tools/parcoach/.git"):
        #    subprocess.run("cd tools/parcoach && git pull &&  cd ../..", shell=True, check=True)
        #else:
        #    subprocess.run("rm -rf tools/parcoach && git clone --depth=1 https://github.com/parcoach/parcoach.git tools/parcoach", shell=True, check=True)

        #subprocess.run("ln -s $(which clang) /usr/lib/llvm-9/bin/clang", shell=True, check=True)
        subprocess.run(f"wget https://gitlab.inria.fr/api/v4/projects/12320/packages/generic/parcoach/2.4.0/parcoach-2.4.0-shared-Linux.tar.gz", shell=True, check=True)
        subprocess.run(f"tar xfz parcoach-*.tar.gz", shell=True, check=True)
        if not os.path.exists("/usr/lib/llvm-15/bin/clang"):
            subprocess.run("ln -s $(which clang) /usr/lib/llvm-15/bin/clang", shell=True, check=True)

        # Go to where we want to install it, and build it out-of-tree (we're in the docker)
        subprocess.run(f"rm -rf {rootdir}/builds/parcoach && mkdir -p {rootdir}/builds/parcoach", shell=True, check=True)
        subprocess.run(f"mv parcoach-*/ {rootdir}/builds/parcoach/ ", shell=True, check=True)
        os.environ['PATH'] = os.environ['PATH'] + f":/{rootdir}/builds/parcoach/parcoach-2.4.0-shared-Linux/bin/"
        os.environ['OMPI_CC'] = "clang-15"

        #os.chdir(f'{rootdir}/builds/parcoach')
        #subprocess.run(f"cmake {rootdir}/tools/parcoach -DCMAKE_C_COMPILER=clang -DLLVM_DIR={rootdir}/tools/Parcoach/llvm-project/build", shell=True, check=True)
        #subprocess.run("make -j$(nproc) VERBOSE=1", shell=True, check=True)

        # Back to our previous directory
        os.chdir(here)

    def run(self, execcmd, filename, binary, id, timeout, batchinfo):
        cachefile = f'{binary}_{id}'
        if filename.find('Win')>=0:
            run_cmd(
                    #buildcmd=f"clang -c -g -emit-llvm {filename} -I/usr/lib/x86_64-linux-gnu/mpich/include/ -o {binary}.bc",
                    #execcmd=f"opt-9 -load ../../builds/parcoach/src/aSSA/aSSA.so -parcoach -check-mpi {binary}.bc -o /dev/null",
                    buildcmd=f"parcoachcc -check=rma --args mpicc {filename} -c -o {binary}.o",
                    execcmd=f"parcoachcc -check=rma --args mpicc {filename} -c -o {binary}.o",
                    cachefile=cachefile,
                    filename=filename,
                    binary=binary,
                    timeout=timeout,
                    batchinfo=batchinfo)
        else:
            run_cmd(
                    buildcmd=f"parcoachcc mpicc {filename} -c -o {binary}.o",
                    execcmd=f"parcoachcc mpicc {filename} -c -o {binary}.o",
                    cachefile=cachefile,
                    filename=filename,
                    binary=binary,
                    timeout=timeout,
                    batchinfo=batchinfo)


        subprocess.run("rm -f *.bc core", shell=True, check=True)

    def parse(self, cachefile):
        if os.path.exists(f'{cachefile}.timeout') or os.path.exists(f'logs/parcoach/{cachefile}.timeout'):
            outcome = 'timeout'
        if not (os.path.exists(f'{cachefile}.txt') or os.path.exists(f'logs/parcoach/{cachefile}.txt')):
            return 'failure'

        with open(f'{cachefile}.txt' if os.path.exists(f'{cachefile}.txt') else f'logs/parcoach/{cachefile}.txt', 'r') as infile:
            output = infile.read()

        if re.search('MBI_MSG_RACE', output):
            return 'MBI_MSG_RACE'
        if re.search('Local concurrency', output):
            return 'local concurrency'
        if re.search('warning', output):
            return 'deadlock'


        if re.search('Compilation of .*? raised an error \(retcode: ', output):
            return 'UNIMPLEMENTED'

        #if re.search('0 warning\(s\) issued', output):
        if re.search('No issues found', output):
            return 'OK'

        if re.search('missing info for external function', output):
            return 'UNIMPLEMENTED'

        return 'other'
