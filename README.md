# PARCOACH Versions Comparison on MBI

This repository contains updtated versions of [MBI v1.0.0](https://gitlab.com/MpiBugsInitiative/MpiBugsInitiative/-/commit/4ec1c8c4e66b264ebe44d74cbd9c353dcbd6b876) (commit ec1c8c4)
to respectively use PARCOACH v1.2 and PARCOACH v2.4.

PARCOACH is automatically installed in the Docker image provided
by MBI. We changed the script parcoach.py in /script/tools/ to
use the two versions of PARCOACH we wanted to compare. The
Dockerfile was also updated to get the right version of LLVM.


To launch PARCOACH, go to one of the folder, then build and run the Docker image:

```bash
# Don't forget to cd into one of the versions folder before doing this!
docker build -f Dockerfile -t mbi:latest .
# This runs a container with the image, and mount the current directory into the container at /MBI.
docker run -v `pwd`:/MBI -it mbi:latest bash
```

You probably want to set specific tags for each image (such as `mbi:parcoach-1.2` instead of the generic `latest` you would use to build the most recent docker image for MBI).

The following steps are the same for testing both versions of PARCOACH: once in the container, generate the codes with the command
```bash
python3 /MBI/MBI.py -c generate
```

and launch PARCOACH
```bash
python3 /MBI/MBI.py -x parcoach -c run
```

To retrieve the results presented in the paper, you can use the following command
```bash
python3 /MBI/MBI.py -x parcoach -c latex
```



## Dashboard


An online version of the results can be consulted here: https://parcoach.gitlabpages.inria.fr/versions-comparison-on-MBI.

You can also checkout the following folder to get/regenerate the results for each version:
  - [Results for PARCOACH v1.2](https://gitlab.inria.fr/parcoach/versions-comparison-on-MBI/-/tree/main/MpiBugsInitiative-Parcoachv1.2)
  - [Results for PARCOACH v2.4.0](https://gitlab.inria.fr/parcoach/versions-comparison-on-MBI/-/tree/main/MpiBugsInitiative-Parcoachv2.4)
